#!/usr/bin/env python

#------------------------------------------------------------------

import os
import sys
import time
import datetime
import json
import mariadb
import requests
import pika

sys.stdout.reconfigure(line_buffering=True)

#------------------------------------------------------------------
# This is the main starting point for this file.

# find and read the config file.
mypath = os.path.realpath (__file__)
mydir = os.path.dirname (mypath)
myconfig = mydir + "/cicd-config.py"
exec (compile (filename=myconfig, source=open (myconfig).read (), mode="exec"))

series = sys.argv[1];
queue = sys.argv[2];

sresp = requests.get (patchworkURL + '/api/1.2/series/' + series + '/')
if sresp.status_code != 200:
    print ("Error ");
    print (sresp);

payload = sresp.json()

event = {}
event['type'] = 'PATCH'
event['data'] = payload
event['series_id'] = series
event['mbox_url'] = patchworkURL + '/series/' + series + '/mbox'

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
rabbitmq_channel = connection.channel()

rabbitmq_channel.queue_declare (queue = queue, durable = True);

rabbitmq_channel.basic_publish (exchange = '',
                                routing_key = queue,
                                body = json.dumps(event));
